﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.Entities;

namespace Demo.Services
{
    public interface IChemicalElementService
    {
        public Task<List<Chemicalelement>> Fetch();
        public Task<Chemicalelement> Find(int id);
        public Task<int> Delete(int id);

    }
}
