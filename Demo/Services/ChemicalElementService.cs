﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.Entities;
using Demo.Repositories;
namespace Demo.Services
{
    public class ChemicalElementService : IChemicalElementService
    {
        private IChemicalElementRepository chemicalElementRepository;
        public ChemicalElementService(IChemicalElementRepository chemicalElementRepository)
        {
            this.chemicalElementRepository = chemicalElementRepository;
        }
        public Task<List<Chemicalelement>> Fetch()
        {
            var list = this.chemicalElementRepository.Fetch();
            return list;
        }
        public Task<Chemicalelement> Find(int id)
        {
            var item = this.chemicalElementRepository.Find(id);
            return item;
        }
        public Task<int> Delete(int id)
        {
            var result = this.chemicalElementRepository.Delete(id);
            return result;
            
        }
    }
}
