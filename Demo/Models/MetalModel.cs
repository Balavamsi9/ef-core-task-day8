﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Demo.Models
{
    public class MetalModel
    {
        public int Id { get; set; }
        public int? ChemicalElement { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
    }
}
