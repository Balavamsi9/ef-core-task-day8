﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.Entities;
using Microsoft.EntityFrameworkCore;
namespace Demo.Repositories
{
    public class ChemicalElementRepository : IChemicalElementRepository
    {
        private ScienceDemoContext demoContext;
        public ChemicalElementRepository(ScienceDemoContext demoContext)
        {
            this.demoContext = demoContext;
        }
        public Task<List<Chemicalelement>> Fetch()
        {
            var list = this.demoContext.Chemicalelements.ToListAsync();
            return list;
        }
        public Task<Chemicalelement> Find(int id)
        {
            var item = this.demoContext.Chemicalelements.Where(x => x.Id == id).FirstOrDefaultAsync();
            return item;
        }
        public async Task<int> Delete(int id)
        {
            try
            {
                var item = await this.demoContext.Chemicalelements.Where(x => x.Id == id).FirstOrDefaultAsync();
                this.demoContext.Remove(item);
                var count = await this.demoContext.SaveChangesAsync();
                return count;

            }
            catch (Exception ex)
            {
                return 0;
            }
            
        }
    }
}
