﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Demo.Entities
{
    public partial class Chemicalelement
    {
        public Chemicalelement()
        {
            Metals = new HashSet<Metal>();
        }

        public int Id { get; set; }
        public string Symbol { get; set; }
        public string Name { get; set; }
        public int? AtomicWeight { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }
        public int? Electorn { get; set; }

        public virtual ICollection<Metal> Metals { get; set; }
    }
}
