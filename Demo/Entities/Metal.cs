﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Demo.Entities
{
    public partial class Metal
    {
        public int Id { get; set; }
        public int? ChemicalElement { get; set; }
        public DateTime? CreatedTime { get; set; }
        public DateTime? UpdatedTime { get; set; }

        public virtual Chemicalelement ChemicalElementNavigation { get; set; }
    }
}
