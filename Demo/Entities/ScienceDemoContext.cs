﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Demo.Entities
{
    public partial class ScienceDemoContext : DbContext
    {
        public ScienceDemoContext()
        {
        }

        public ScienceDemoContext(DbContextOptions<ScienceDemoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Chemicalelement> Chemicalelements { get; set; }
        public virtual DbSet<Metal> Metals { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySQL("Server=localhost;Database=ScienceDemo;user id=root;password=sysadmin;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Chemicalelement>(entity =>
            {
                entity.ToTable("chemicalelements");

                entity.HasIndex(e => e.Name, "Index_Science_Name");

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Name).HasMaxLength(255);

                entity.Property(e => e.Symbol).HasMaxLength(5);
            });

            modelBuilder.Entity<Metal>(entity =>
            {
                entity.ToTable("metals");

                entity.HasIndex(e => e.ChemicalElement, "ChemicalElement");

                entity.HasOne(d => d.ChemicalElementNavigation)
                    .WithMany(p => p.Metals)
                    .HasForeignKey(d => d.ChemicalElement)
                    .HasConstraintName("metals_ibfk_1");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
