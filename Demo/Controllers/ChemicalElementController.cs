﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.Models;
using Demo.Entities;

namespace Demo.Controllers
{
    [Route("api/chemical-element")]
    [ApiController]
    public class ChemicalElementController : ControllerBase
    {
        private ScienceDemoContext demoContext;
        public ChemicalElementController(ScienceDemoContext demoContext)
        {
            this.demoContext = demoContext;
        }

        [HttpGet]
        [Route("fetch")]
        public IActionResult Fetch()
        {
            var list = this.demoContext.Chemicalelements.ToList();
            return Ok(list);
        }
        [HttpGet]
        [Route("find")]
        public IActionResult Find(string name)
        {
            var item = this.demoContext.Chemicalelements.Where(x => x.Name == name).FirstOrDefault();
            return Ok(item);
        }
        [HttpPost]
        [Route("create")]
        public IActionResult Create(ChemicalelementModel obj)
        {
            var entity = new Chemicalelement
            {
                Id = obj.Id,
                Symbol = obj.Symbol,
                Name = obj.Name,
                AtomicWeight = obj.AtomicWeight,
                CreatedTime = obj.CreatedTime,
                UpdatedTime = obj.UpdatedTime
            };
            this.demoContext.Chemicalelements.AddAsync(entity);
            this.demoContext.SaveChangesAsync();
            return Ok("Chemical Element Added Successfully");
        }
        [HttpPut]
        [Route("update")]
        public IActionResult Update(ChemicalelementModel obj)
        {
            var item = this.demoContext.Chemicalelements.Where(x => x.Id == obj.Id).FirstOrDefault();
            item.Symbol = obj.Symbol;
            item.Name = obj.Name;
            item.AtomicWeight = obj.AtomicWeight;
            item.UpdatedTime = obj.UpdatedTime;
            this.demoContext.SaveChangesAsync();
            return Ok("Chemical Element Updated Successfully");
        }
        [HttpDelete]
        [Route("delete")]
        public IActionResult Delete(int id)
        {
            var item = this.demoContext.Chemicalelements.Where(x => x.Id == id).FirstOrDefault();
            this.demoContext.Remove(item);
            this.demoContext.SaveChangesAsync();
            return Ok("Chemical Element Deleted Successfully");

        }



    }

}
