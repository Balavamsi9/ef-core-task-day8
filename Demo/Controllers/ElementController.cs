﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Demo.Services;
using Demo.Repositories;
using Demo.Entities;

namespace Demo.Controllers
{
    [Route("api/element")]
    [ApiController]
    public class ElementController : ControllerBase
    {
        //private ScienceDemoContext demoContext;
        private IChemicalElementService service;
        //private IChemicalElementRepository repository;

        public ElementController(IChemicalElementService service)
        {
            //this.demoContext = new ScienceDemoContext();
            //this.repository = new ChemicalElementRepository(demoContext);
            //this.service = new ChemicalElementService(repository);
            this.service = service;
            
        }
        [HttpGet]
        [Route("fetch")]
        public async Task<IActionResult> Fetch()
        {
            try
            {
                var list = await this.service.Fetch();
                return Ok(list);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpGet]
        [Route("find")]
        public async Task<IActionResult> Find(int id)
        {
            try
            {
                var item = await this.service.Find(id);
                return Ok(item);

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpDelete]
        [Route("delete")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var result = await this.service.Delete(id);
                if (result >= 1)
                {
                    return Ok("Chemical Element Deleted Successfully");
                }
                else
                {
                    return Ok("You Have Entered Invalid ID-cant able to delete ");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
    
            }
        }


    }
}
